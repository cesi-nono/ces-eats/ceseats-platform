# CES'EATS PLATFORM

## PREREQUEST

Make sur you have docker engine and docker-compose up.

```bash
$ sudo service docker start # or if not working: sudo systemctl start docker
$ docker version
```
If you use Docker Desktop for Windows, please run it.


## HOW TO USE IT

```bash
$ git clone --recursive https://gitlab.com/cesi-nono/ces-eats/ceseats-platform.git
$ cd ceseats-platform
$ docker-compose up -d
```

If you want to rebuild your images

```bash
$ docker-compose build
```

And to stop your services

```bash
$ docker-compose down
```

### Repull submodules

```bash
$ git submodule update --remote
```

## WHAT'S RUNNING?

This docker-compose file allows you to start all required containers to offer the architecture under.

![Alt text](./architecture.png?raw=true "Architecture")


## CONTAINER LIST

:exclamation: For both networks the default gateway is 192.168.X.254

### FRONTEND (192.168.1.X)

CONTAINER  | PORT (localhost) | Internal IPv4 
------------- | ------------- | -------------
APP Customer | 3000:3000  | 192.168.1.1
APP Professional  | 3005:3000  | 192.168.1.2
APP Delivry  | 3010:3000  | 192.168.1.3
Middleware-platform  | 4000:8080 / (9876:9876)  | 192.168.1.100


### BACKEND (192.168.0.10X)

CONTAINER  | PORT (localhost) | Internal IPv4 
------------- | ------------- | -------------
Middleware-platform  | 4000:8080 / (9876:9876) | 192.168.0.100
Components A  | 4005:3000  | 192.168.0.101
Components B  | 4006:3000  | 192.168.0.102


### BACKEND-DB (192.168.0.15X)

CONTAINER  | PORT (localhost) | Internal IPv4 
------------- | ------------- | -------------
NoSQL Db  | 5010:27017  | 192.168.0.150
SQL Server Db  | 5020:1433  | 192.168.0.151
Redis  | 5030:6379  | 192.168.0.152


### MANAGER (192.168.0.20X)

CONTAINER  | PORT (localhost) | Internal IPv4 
------------- | ------------- | -------------
Swagger-facade  | 6010:8000  | 192.168.0.200
NoSQL Manager  | 6020:8081  | 192.168.0.201
Grafana  | 6030:3000  | 192.168.0.202


### OPTIONAL (192.168.0.25X)

CONTAINER  | PORT (localhost) | Internal IPv4 
------------- | ------------- | -------------
Debian Front Network  | -  | 192.168.1.253
Debian Back Network  | -  | 192.168.0.253
Debian in both network  | -  | 192.168.0.252 / 192.168.1.252